<h1 align="center">Hi 👋, I'm Hugh</h1>
<h3 align="center">A SW generalist</h3>

- 💬 Ask me about **python**

- 📫 How to reach me **deeoh at woodforsheep.trade**

- 📄 Know about my experiences [http://linkedin.com/in/henxing](http://linkedin.com/in/henxing)

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://linkedin.com/in/henxing" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/linkedin.svg" alt="henxing" height="30" width="40" /></a>
<a href="https://letterboxd.com/deeoh22/" target="blank"><img align="center" src="https://a.ltrbxd.com/logos/letterboxd-decal-dots-pos-rgb.svg" alt="deeoh22" height="40" width="40" /></a>
<a rel="me" href="https://social.horrorhub.club/@deeoh" target="blank"><img align="center" src="https://raw.githubusercontent.com/mastodon/mastodon/fe2d6fe1057677a1060fe1d882aac744456deff4/public/favicon.ico" alt="deeoh" height="40" width="40" /></a>
</p>

<h3 align="left">Languages and Tools:</h3>
<p align="left">
<a href="https://www.gnu.org/software/bash/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt="bash" width="40" height="40"/> </a>
<a href="https://www.cprogramming.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a>
<a href="https://www.w3schools.com/cpp/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" height="40"/> </a>
<a href="https://www.djangoproject.com/" target="_blank"> <img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/django.svg" alt="django" width="40" height="40"/> </a>
<a href="https://git-scm.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a>
<a href="https://helm.sh/" target="_blank"> <img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/helm.svg" alt="" width="40" height="40"/> </a>
<a href="https://www.elastic.co/kibana" target="_blank"> <img src="https://www.vectorlogo.zone/logos/elasticco_kibana/elasticco_kibana-icon.svg" alt="kibana" width="40" height="40"/> </a>
<a href="https://kubernetes.io" target="_blank"> <img src="https://www.vectorlogo.zone/logos/kubernetes/kubernetes-icon.svg" alt="kubernetes" width="40" height="40"/> </a>
<a href="https://www.linux.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a>
<a href="https://neovim.io/" target="_blank"> <img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/neovim.svg" alt="neovim" width="40" height="40"/> </a>
<a href="https://opencv.org/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/opencv/opencv-icon.svg" alt="opencv" width="40" height="40"/> </a>
<a href="https://www.python.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a>
<a href="https://pre-commit.com/" target="_blank"> <img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/pre-commit.svg" alt="pre-commit" width="40" height="40"/> </a>
<a href="https://github.com/tmux/tmux/wiki" target="_blank"> <img src="https://raw.githubusercontent.com/tmux/tmux/f68d35c52962c095e81db0de28219529fd6f355e/logo/favicon.ico" alt="tmux" width="40" height="40"/> </a>
</p
